package com.example.SpringDemo.student;

import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.time.LocalDate;
import java.time.Month;
import java.util.List;

@Configuration
public class StudentConfig {
    @Bean
    CommandLineRunner commandLineRunner(StudentRepository repository){
        return  args -> {
            Student hari = new Student(
                    "Hari",
                    "hkholla@gmail.com",
                    LocalDate.of(2000, Month.JANUARY,5)
            );
            Student hari2 = new Student(
                    "Hari2",
                    "hkholla2@gmail.com",
                    LocalDate.of(2004, Month.JANUARY,5)
            );

            repository.saveAll(
                    List.of(hari,hari2)
            );
        };
    }
}
